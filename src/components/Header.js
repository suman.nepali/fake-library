import React, { useContext } from "react";

import AdminContext from "../contexts/AdminContext.js";

const Header = () => {
    const { admin, setAdmin } = useContext(AdminContext);

    const handleAdminChange = () => {
        setAdmin(!admin);
    };

    return (
        <div className="header">
            <button onClick={handleAdminChange}>{admin ? "Admin" : "Customer"}</button>
        </div>
    )
}

export default Header;
