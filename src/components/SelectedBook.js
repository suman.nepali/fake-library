
import React, { useContext } from "react";
import { useParams } from "react-router-dom"; 
import "./SelectedBook.css";
import allBooks from "../db.json";
import Header from "./Header";
import AdminContext from "../contexts/AdminContext.js";


function SelectedBook () {
    const params = useParams();
    console.log("useparams hook", params);
    const {admin} = useContext(AdminContext);
    console.log(admin);
    const book = allBooks.filter(books => books.isbn === params.isbn
        );
    
    return(
        <div className="main-container">
            <div className="child-container">
                <ul style={{listStyle: "none"}}>
                    <li key = {1}><b>Title:</b> {book[0].title}</li>
                    <li key = {2}>S<b>ubtitle:</b> {book[0].subtitle}</li>
                    <li key = {3}><b>Author:</b> {book[0].author}</li>
                    <li key = {4}><b>Published:</b> {book[0].published}</li>
                    <li key = {5}><b>Publisher:</b>{book[0].publisher}</li>
                    <li key = {6}><b>Pages:</b> {book[0].pages}</li>
                    <li key = {7}><b>Description:</b> {book[0].description}</li>
                    <li key = {8}><b>Website:</b> {book[0].website}</li>
                    <li> 
                        {admin ? <div className="btnsForSelectedBook"><button>Edit</button> | <button>Delete</button></div> : <div><button>Reserve</button> | <button>Borrow</button></div>}
                    </li>
                    
                </ul>
            </div>
        </div>
    );
}

export default SelectedBook;

