
import React, { useState, useMemo } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import AdminContext from "./contexts/AdminContext.js";
import DataContext from "./contexts/DataContext.js";
import data from "./contexts/data.js";

import Base from "../src/views/Base.js";
import ViewAllBooks from "../src/views/ViewAllBooks"
import SelectedBook from "./components/SelectedBook.js";

import './App.css';

function App() {
  const [admin, setAdmin] = useState(false);
  const value = useMemo(
    () => ({ admin, setAdmin }),
    [admin]
  );

  return (
    <BrowserRouter>
      <AdminContext.Provider value={{ admin, setAdmin }}>
        <DataContext.Provider value={ data }>
          <Routes>
            <Route path="/" element={<Base />} >
              <Route path="books" element={<ViewAllBooks />}/>
              <Route path="book/:isbn" element={<SelectedBook />} />
              <Route path="newBook" element={<AddNewBookViews />} />
            </Route>
          </Routes>
        </DataContext.Provider>
      </AdminContext.Provider>
    </BrowserRouter>
  );
}

export default App;
