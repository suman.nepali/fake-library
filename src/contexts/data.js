import axios from "axios";

const data = {
    getAllBooks: async () => {
        const { data } = await axios.get("http://localhost:3001/books");
        return data;
    },

    getBook: async (isbn) => {
        const { data } = await axios.get("http://localhost:3001/books/" + isbn);
        return data;
    },

    saveBook: async (book) => {
        const res = await axios.post("http://localhost:3001/books", book);
        return res;
    },

    updateBook: async (isbn, book) => {
        const res = axios.put(`http://localhost:3001/books/${isbn}`, book);
        return res;
    }
};

export default data;
