import React from "react";
import { Outlet } from "react-router-dom";

import Header from "../components/Header";

const Base = () => {

  return (
    <div className="base">
      <Header />
      <Outlet />
    </div>
  )
};

export default Base;