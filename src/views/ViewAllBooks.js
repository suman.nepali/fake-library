import React, { useContext, useEffect, useState } from "react";
import './ViewAllBooks.css';
import { Link } from "react-router-dom";
import DataContext from "../contexts/DataContext.js";
import AdminContext from "../contexts/AdminContext.js" 

const ViewAllBooks = () => {

    const data = useContext(DataContext);

    const [books, setBooks] = useState([]);

    useEffect(() => {        
        const booksData = async () => {
            const books = await data.getAllBooks();

            setBooks(books);         
        }
        booksData();
        
    }, []);
      
    const {admin} = useContext(AdminContext);
    

    const booksListed = books.map(book =>
        <div>
            <p className="booklist"><b>Title:</b> {book.title};<br/><b>Author:</b> {book.author};<br/> <b>Availability:</b> {book.availability};</p>
            <Link
                className="linkToBook"
                key={book.isbn}
                to={`/book/${book.isbn}`}>Book Details
            </Link>
        </div>
    )
    return (
        <div>
            <h2 className="heading">Here is the list of books in our library. Click on "Book Details" for additional information.</h2>
            <div className="wrapper">
                {booksListed}
            </div>
            <div>
                {(admin) ? <Link className="linkToNew" to={`/newbook`}> Add a new book </Link>: null}
            </div>
        </div>


    );
}

export default ViewAllBooks;