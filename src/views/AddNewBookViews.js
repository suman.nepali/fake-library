import React from "react";
import { useState, useContext } from "react";
import DataContext from "../contexts/DataContext.js";

const AddNewBookViews = () => {

    const newBookData = useContext(DataContext);

    const [book, setBook] = useState({
        //input default value
        isbn: 0,
        title: "",
        subtitle: "",
        author: "", 
        published: "", 
        publisher: "", 
        pages: 0, 
        description: "", 
        website: ""   
    })

    const handleSubmit = (event) => {
        event.preventDefault();
        const bookObject = {
            // object values
            isbn: event.target[0].value,
            title: event.target[1].value,
            subtitle: event.target[2].value,
            author: event.target[3].value,
            published: event.target[4].value,
            publisher: event.target[5].value,
            pages: event.target[6].value,
            description: event.target[7].value,
            website: event.target[8].value,
        }


        setBook(bookObject);
        newBookData.saveBook(bookObject);
   
        console.log(bookObject);
    }

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="">ISBN </label>
            <input type="number" name="isbn" id="isbn" required />
            <br />

            <label htmlFor="">Title </label>
            <input type="text" name="title" id="title" required />
            <br />

            <label htmlFor="">Subtitle </label>
            <input type="text" name="subtitle" id="subtitle" />
            <br />

            <label htmlFor="">Author </label>
            <input type="text" name="author" id="author" required />
            <br />

            <label htmlFor="">Published </label>
            <input type="date" name="published" id="published" required />
            <br />

            <label htmlFor="">Publisher </label>
            <input type="text" name="publisher" id="publisher" required />
            <br />

            <label htmlFor="">Pages </label>
            <input type="number" name="pages" id="pages" required />
            <br />

            <label htmlFor="">Description</label>
            <br />
            <textarea name="description" id="description" cols="30" rows="10" required></textarea>
            <br />

            <label htmlFor="">Website </label>
            <input type="text" name="website" id="website" />
            <br />

            <input type="submit" value="Add new book" />
        </form>
    )
};


export default AddNewBookViews;